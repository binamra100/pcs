<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PCS</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="<spring:url value="/resources/css/home.css"/>" type="text/css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>
	<jsp:include page="fragments/header.jsp"></jsp:include>
	
<div class="container">
		<div class="row">
		
			<form action="<spring:url value="/save"/>" method="post" class="col-md-8 col-md-offset-2">
			
			
				<div class="form-group">
					<label for="id">ID</label>
					<input type="text" id="name" 
							class="form-control" name="id"/>
				</div>
				
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" id="name" 
							class="form-control" name="name"/>
				</div>

							
			
				<div class="form-group">
					<label for="description">Description</label>
					<input type="text" id="name" 
							class="form-control" name="description"/>
				</div>
				
				
			
				<button type="submit" class="btn btn-default">Submit</button>
	
			</form>


	
	
</body>
</html>