package com.binamra.kandel.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.binamra.kandel.data.entities.Student;
import com.binamra.kandel.data.entities.StudentDao;


@Controller
public class HomeController {
	@Autowired  
    StudentDao dao;//will inject dao from xml file  
	@RequestMapping("/")
	public String goHome(Model model){
		return "home";
	}
	
	@RequestMapping(value="/add")
	public String addStudent(){
		return "add";
		
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView saveStudent(@ModelAttribute Student student){
			dao.save(student);  
        return new ModelAndView("redirect:/show");
	}
	
	@RequestMapping("/show")
	public ModelAndView showStudent(){
		List<Student> list = dao.getStudents();
		return new ModelAndView("show","students",list);
	}
	
	 
    @RequestMapping(value="/editstd/{id}")  
    public ModelAndView edit(@PathVariable int id){  
        Student s =dao.getEmpById(id);  
        return new ModelAndView("editform","command",s);  
    }  
    
    
    @RequestMapping(value="/editstd/editsave",method = RequestMethod.POST)  
    public ModelAndView editsave(@ModelAttribute("student") Student student){  
        dao.update(student);  
        return new ModelAndView("redirect:/show");  
    } 
	
	  @RequestMapping(value="/delete/{id}",method = RequestMethod.GET)  
	    public ModelAndView delete(@PathVariable int id){  
	        dao.delete(id);  
	        return new ModelAndView("redirect:/show");  
	    }  
}
